package com.shujia

object Demo14Set {


  def main(args: Array[String]): Unit = {

    val set: Set[Int] = Set(1, 2, 3, 4, 5, 6, 7, 8, 1)
    println(set)

    println(set.mkString(","))
    println(set.max)
    println(set.min)

    //和顺序相关的方法set 没有


    val s1 = Set(1, 2, 3, 4, 5)
    val s2 = Set(4, 5, 6, 7, 8)

    //& 是一个方法
    println(s1 & s2) //计算集合的交集
    println(s1 | s2) // 计算两个集合的并集
    println(s2 &~ s1) //差集


  }

}
