package com.shujia

object Demo9Fun2 {
  def main(args: Array[String]): Unit = {

    /**
      * lambda表达式
      *
      * (s: String) => s.toInt
      * =>的左边是参数
      * =>的右边是返回值，返回值类型会自动推断
      */

    val f: String => Int = (s: String) => s.toInt

    def sToInt(f:String => Int) = {
      println(f("1000")*2)
    }


    /**
      * 使用lambda表达式调用函数
      * s参数名
      */
    sToInt((s: String) =>{
      s.toInt
    })

    //如果lambda d表达式的函数体之后一行，可以省略大括号
    sToInt((s: String) => s.toInt)

    //lambda 表达式参数的类型可以自动推断
    sToInt(s => s.toInt)

    //如果lambda 表达式的参数只使用了一次可以通过下划线代替
    sToInt(_.toInt)



  }

}
