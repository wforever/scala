package com.shujia

object Demo5Class {

  def main(args: Array[String]): Unit = {
    val student = new sStudent(1001,"张三")
    println(student)

    val student2 = new sStudent(1002,"李四",23)
    println(student2)

    val student_1 = new Student_1()
    println(student_1)

    val student_2 = new Student_1("haha")
    println(student_2)

  }

}

class sStudent(id: Int ,name: String){

  //定义属性并赋值
  private val _id: Int= id
  val _name: String = name

  //下划线起到占位作用
  var _age: Int = _

  //自定义方法
  def getId(): Int = {
    _id
  }

  //override 方法重写
  override def toString(): String = "Student("+_id+","+_name+","+_age+")"

  //重载构造函数
  //在重载构造函数的第一行需要显示调用默认构造函数
  def this(id: Int,name: String,age: Int){
    this(id,name)
    this._age = age
  }



}

class Student_1(){

  private var name:String = _


  override def toString(): String = "Student("+name+")"

  def this(name: String){
    this()
    this.name = name
  }
}
