package com.shujia

import scala.io.Source

object Demo21ImplicitClass {

  def main(args: Array[String]): Unit = {


    val words: List[String] = new FileRead("data/words.txt").read()

    words.foreach(println)

    /**
      * 隐式转换
      * 动态给对象增加新的方法
      *
      */
    val strings: List[String] = "data/words.txt".read()
    strings.foreach(println)


  }

  /**
    * 隐式转换类
    * 可以将构造函数参数的类型隐式转换当前类
    *
    */
  implicit class FileRead(path: String){
    def read(): List[String] = {
      Source.fromFile(path).getLines().toList
    }
  }

}
