package com.shujia

object Demo18Match {


  def main(args: Array[String]): Unit = {

    /**
    java中的模式匹配可以匹配，基本数据类型，字符串，枚举
      *
      * scala中的模式匹配，可以匹配基本数据类型，字符串，枚举，匹配对象，匹配类型
      *
      * 按照顺序匹配，只会匹配成功一个
      * 如果前面没有匹配成功会走下划线
      *
      */
    //------------1、匹配基本数据类型------------------
    val i = 100

    i match {
      case 1 => println("i = 100")
      case 100 => println("i = 100")
      case _ => println("I not find")
    }

    //-----------2、匹配字符出串----------------------
    val str = "java"

    str match {
      case "java" => println("java")
      case "scala" => println("scala")
      case "python" => println("python")
      case _ => println("未知语言")
    }

    //----------3、匹配对象----------------------------
    case class User(id: String,name: String)

    val user: User = User("20210708","张三")

    user match {
      case User("20210708","张三") => println("张三")
      case User("20210809","李四") => println("李四")
      case User("20210511","王五") => println("王五")
      case _ => println("匹配失败")
    }

    //----------4、匹配类型----------------------------

    val j: Any = 100.1

    j match {
      //这里的i  代表的是上面的j  执行类型自动做了转换
      case i: Int => println("i是Int类型"+i)
      case s: String => println("i是String类型"+s)
      case _ => println("其他类型"+i)
    }

    //---------匹配成功之后可以有返回值------------------

    val k = 100

    val r: Int = k % 2 match {
      case 1 => k + 1
      case 0 => k * 2
    }

    println(r)

    //------------模式匹配的应用------------------------

    val map: Map[String, String] = Map("001" -> "张三", "002" -> "李四")

    val name: String = map.getOrElse("001", "默认值")

    println(name)

    /**
      * Option ; 由两个取值，Some  None
      * some代表有值
      * None 代表没有值
      *
      */

    val option: Option[String] = map.get("003")

    val value: String = option match {
      //如果有值返回对应的值
      case Some(v) => v
      //如果没有值返回默认值
      case None => "默认值"
    }

    println(value)

    //--------------------------------------------------------

    val list: List[(String, Int)] = List(("001", 23), ("002", 24), ("003", 25))

    val list2: List[(String, Int)] = list.map(kv => {
      val id: String = kv._1
      val age: Int = kv._2
      (id,age+1)
    })

    // list2 = List((001,24), (002,25), (003,26))
    println(list2)

    /**
      * 使用case 代替lambda 表达式
      *
      * 需要将小括号替换成大括号
      */

    val list3: List[(String, Int)] = list2.map{
      //匹配集合中的数据是否满足指定类型
      case (id: String,age: Int) =>
      (id,age+1)
    }

    println(list3)

  }

}
