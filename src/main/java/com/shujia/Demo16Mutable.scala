package com.shujia

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Demo16Mutable {

  def main(args: Array[String]): Unit = {

    //可变list
    val listBuffer = new ListBuffer[Int]

    //往集合中添加元素
    listBuffer += 1
    listBuffer += 2
    listBuffer += 3
    listBuffer += 4
    listBuffer += 5
    println(listBuffer)
    //一次添加多个元素
    listBuffer ++= List(9,8,7,6)
    println(listBuffer)

    //根据下标插入元素
    listBuffer.insert(5,20)
    println(listBuffer)

    //指定下标更新元素
    listBuffer.update(2, 20)
    println(listBuffer)

    //删除指定元素，若有多个只删除一个
    listBuffer.-=(20)
    println(listBuffer)

    //通过下标获取元素
    println(listBuffer(1))


    //list 中有的方法 listBuffer 中都有


    //类型转换
    val list: List[Int] = listBuffer.toList
    println(list)

    val hashMap = new mutable.HashMap[String,Int]()

    hashMap .+=(("001",56))
    hashMap += (("002",21),"003" → 23)
    println(hashMap)

    //删除元素
    hashMap.remove("002")
    hashMap.-=("003")

    println(hashMap)

    //更新元素，如果元素不存在，增加元素
    hashMap.update("003", 29)
    println(hashMap)
    hashMap("004") = 20
    println(hashMap)

    //map有的方法 HashMap 都有

    val hashSet = new mutable.HashSet[Int]()

    //增加删除元素
    hashSet += 1
    hashSet += 2
    hashSet += 2
    hashSet -= 1

    hashSet.add(4)
    hashSet.remove(4)

    println(hashSet)



  }



}
