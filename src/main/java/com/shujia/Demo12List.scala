package com.shujia

object Demo12List {

  def main(args: Array[String]): Unit = {

    /**
      * 相当于java中的ArrayList
      * scala中默认的list是不可变的
      */

    val list: List[Int] = List(7,3,5,2,4,6,1)


    println(list(1)) //通过下标取值
    println(list.mkString(",")) //拼接成字符串
    println(list.head) //取第一个元素
    println(list.last) //取最后一个元素
    println(list.tail) //取除了第一个元素之后的所有元素
    println(list.take(4)) //取前几个元素
    println(list.distinct) //去重
    println(list.reverse) //反转集合，返回一个新的集合
    println(list.max) //取最大
    println(list.min) //取最小
    println(list.sum) //求和
    println(list.size)
    println(list.length)


    /**
      * 高级方法
      */

    /**
      * map 函数
      * 将集合中的元素一个一个传递给后面的函数，最终返回一个新的集合
      * 数据行数不变，原来是多少行处理完还是多少行
      *
      */
    val list1: List[Int] = list.map((i: Int) => i*2)
    println(list1)


    /**
      * filter函数，多集合中元素进行过滤
      *后面的函数返回true 保留数据
      * 后面的函数返回false 过滤数据
      * 元素数量会减少
      */

    val list2: List[Int] = list.filter((i: Int) => i%2==1)
    println(list2)

    /**
      * sort 函数
      *
      * sortBy: 通过某一个字段进行排序，默认是升序,-i为降序
      * sortWith: 传入一个比较规则进行排序
      *
      */
    val list3: List[Int] = list.sortBy((i: Int) => i)
    println(list3)

    val list4: List[Int] = list.sortWith((i: Int,j: Int) => i>j)
    println(list4)



    val str1 = List("java,spark,hadoop,java", "hive,scala,hbase", "hive,scala,hbase", "flume,sqoop")
    val words: List[String] = str1.flatMap((lines: String) => lines.split(","))
    println(words)

    /**
      * 结果数据不变--- map
      * 结果数据变少--- filter
      * 结果数据变多 -- flatMap
      *
      */

    /**
      *
      * groupBy  函数
      *
      * 通过某一个列进行分组，将同一个列分到同一个组内
      *
      */
    val map: Map[String, List[String]] = words.groupBy((word: String) => word)
    for (elem <- map) {
      println(elem)
    }

    println("=" * 100)

    //简写
    list.foreach(i => println(i))

    println("=" * 100)

    list.foreach(println(_))

    println("=" * 100)

    //多态
    list.foreach(println)



  }

}
