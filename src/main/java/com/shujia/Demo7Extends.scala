package com.shujia

object Demo7Extends {

  def main(args: Array[String]): Unit = {

    val a1 = new A(1,"张三")
    a1.print()

    val b1 = new B(2,"李四",25)
    b1.print()

  }

}

class A(id: Int,name: String){

  println("A的默认构造方法")

  var _id = id
  var _name = name

  def print(): Unit ={
    println(_id+"\t"+_name)
  }
}

class B(id: Int,name: String,age: Int) extends A(id,name){

  println("B的默认构造方法")
  val _age = age

  override def print(): Unit = println(_id+","+_name+","+_age)

}
