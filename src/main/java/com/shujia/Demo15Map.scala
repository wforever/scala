package com.shujia

object Demo15Map {


  def main(args: Array[String]): Unit = {
    val map1: Map[String, String] = Map (("001","zs"),"002"->"ls",("003","ww"))

    println(map1)
    //可以通过key 获取value
    //如果key 不存在会报错
    println(map1("003"))
    //如果key 不存在返回默认值
    println(map1.getOrElse("005", "Don't exists"))

    println(map1.keys)
    println(map1.keys)

    val map2: Map[String, Int] = Map(("001",22),"002"->21,"003"→25)
    //将年龄加一

    /**
      * map ； 将map中的元素传给后面的函数，发牛一个新的map 。 函数的参数是一个二元组
      * mapValues： 将map 中所有value 传给后面的函数，返回一个新的map
      *
      */

    val map21: Map[String, Int] = map2.map((kv:(String,Int)) =>{
      val key: String = kv._1
      val value: Int = kv._2
      (key,value+1)
    })
    println(map21)

    var map22: Map[String, Int] = map2.mapValues((value: Int) => value+1)
    println(map22)



  }


}
