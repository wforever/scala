package com.shujia

object Demo10Fun3 {

  def main(args: Array[String]): Unit = {

    val array = Array(1,2,3,4,5,6)

    println(array.mkString("--"))
    println("-" * 100)

    //scala的高阶函数的使用
    val array1 = Array(1,2,3,4,5,6)

    /**
      * map:一个一个处理数组中的元素
      *
      * 循环数组中的元素，一个一个传给后面的函数，最终返回一个新的数组
      */
    val ints: Array[Int] = array1.map((i: Int) => i*2)
    println(ints.mkString("-"))

    val str1: Array[String] = Array("1.5","1.6","2.3","4.5")
    val dstr1: Array[Double] = str1.map((i: String) => i.toDouble)
    println(dstr1.mkString(","))

    /**
      * 将数组中的奇数乘以二，偶数加1
      */
    val array3 = Array(1,2,3,4,5,6)
    val a3: Array[Int] = array3.map((i: Int) =>{
      if(i%2 == 1){
        i*2
      }else{
        i+1
      }
    })
    println(a3.mkString("//"))

    /**
      * 练习
      * 1、对数组中的元素乘以2  加上100 乘以5
      *
      */
    var a4: Array[Int] = array3.map((i: Int) => {
      (i*2+100)*5
    })
    println(a4.mkString("[]"))


  }

}
