package com.shujia

import java.io.FileNotFoundException

object Demo22Exception {

  def main(args: Array[String]): Unit = {
    try {

      //抛异常
      throw new FileNotFoundException

      //val reader = new FileReader("data/student.txt")
    } catch {
      case e: FileNotFoundException => println("找不到文件的异常")

      case e: RuntimeException => println("运行时异常")

      case _ => println("其它异常")

    } finally {
      println("都会执行")
    }


  }



}
