package com.shujia

object Demo6CaseClass {

  def main(args: Array[String]): Unit = {

    val user = new User(1,"李飒")
    user.name = "十四卡"
    println(user)

    //样例类创建对象可以不用new
    val user2 = User(2,"和借记卡")
    println(user2)
  }


  /**
    * 样例类
    * scala编译器在编译的时候会动太给样例类增加很多方法（toString, hashCode 序列化， 属性）
    *
    *
    * 属性和参数是一样的
    * 属性默认是val  ,是不可变的，改成var 就可以修改了
    *
    */
  case class User(id: Int,var name: String)

}


