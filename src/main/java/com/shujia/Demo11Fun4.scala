package com.shujia


/**
  * 函数作为返回值
  */
object Demo11Fun4 {


  def main(args: Array[String]): Unit = {

    def fun(s: String):String => Int = {

      def f(s1:String) = {
        (s+s1).toInt
      }
      //返回函数：f
      f
    }

    val stringToInt: String => Int = fun("200")
    val i: Int = stringToInt("1")
    println(i)

  }



}
