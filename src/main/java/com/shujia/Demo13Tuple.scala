package com.shujia

object Demo13Tuple {

  def main(args: Array[String]): Unit = {

    /**
      * 元组  ： 有序，不唯一
      * 最多只能由22个元素
      * 可以直接通过下划线加下标取数
      *
      * 解决集合在通过下标取数时可能会出现下标越界的问题
      *
      */
    val t1 = Tuple5(1,2,3,4,5)
    println(t1._4)
  }

}
