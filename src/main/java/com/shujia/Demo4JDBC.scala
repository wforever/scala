package com.shujia

import java.sql.{Connection, DriverManager, PreparedStatement}

import scala.io.{BufferedSource, Source}

object Demo4JDBC {

  def main(args: Array[String]): Unit = {

    Class.forName("com.mysql.jdbc.Driver")
    val conn: Connection = DriverManager
      .getConnection("jdbc:mysql://localhost:3306/students","root","root")
    val ps: PreparedStatement = conn
      .prepareStatement("insert into student values(?,?,?,?,?)")
    val source: BufferedSource = Source.fromFile("data/students.txt")
    val lines: Iterator[String] = source.getLines()
    for (elem <- lines) {
      val str: Array[String] = elem.split(",")
      ps.setInt(1,str(0).toInt)
      ps.setString(2,str(1))
      ps.setInt(3,str(2).toInt)
      ps.setString(4,str(3))
      ps.setString(5,str(4))
      ps.addBatch()

      println(elem)
    }
    ps.executeBatch()
    ps.close()
    conn.close()

  }

}
