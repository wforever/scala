package com.shujia

object Demo20Implicit {

  def main(args: Array[String]): Unit = {


    def fun(s: String): String => Unit ={

      def f(pre: String): Unit = {
        println(pre + "\t" + s)
      }
      f
    }

    fun("java")("前缀")

    //简写
    //函数柯里化
    def fun1(s: String)(s1: String): Unit = {
      println(s + "\t" + s1)
    }

    fun1("scala")("6666")


    //---------------隐式转换参数-------------------

    def fun2(s: String)(implicit s1: String) = {
      println(s + "\t" + s1)
    }

    implicit val y = "默认值"

    fun2("spark")
    fun2("hadoop")

  }



}
