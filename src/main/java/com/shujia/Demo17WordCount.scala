package com.shujia

import java.io

import scala.io.Source

object Demo17WordCount {


  def main(args: Array[String]): Unit = {

    val students: List[String] = Source.fromFile("data/students.txt").getLines().toList
    val clazz: List[String] = students.map((student: String) =>{
      val stu: Array[String] = student.split(",")
      stu(4)
    })

    val clazzMap: Map[String, List[String]] = clazz.groupBy((clazzz: String) => clazzz)
    val jg: Map[String, Int] = clazzMap.mapValues((claz:List[String])=>{
      claz.length
    })
    //jg.foreach(println)
    //println("*" * 100)

    //统计班级人数
    Source
      .fromFile("data/students.txt")
      .getLines
      .toList
      .map(_.split(",")(4))
      .groupBy(a => a)
      .mapValues(_.length)
      //.foreach(println)

    //获取每一行的的值
    val allScore: List[String] = Source.fromFile("data/score.txt").getLines().toList

    //获取各科的得分
    val singleScore: List[(String, Int)] = allScore.map((sScore: String) => {
      val str: Array[String] = sScore.split(",")
      (str(0),str(2).toInt)
    })
    //singleScore.foreach(println)


    //根据学号分组
    val stuScore: Map[String, List[(String, Int)]] = singleScore.groupBy(_._1)

    //求学号+总分
    val cj: Map[String, Int] = stuScore.map(stu => {
      val sId: String = stu._1
      val value: List[(String, Int)] = stu._2

      val ints: List[Int] = value.map(_._2)
      val sumScore: Int = ints.sum
      (sId,sumScore)
    })


    //cj.foreach(println)

    //获取学生的所有信息
    val stu_all: List[(String, String, String, String, String)] = Source
      .fromFile("data/students.txt")
      .getLines
      .toList
      .map(s => {
        val strings: Array[String] = s.split(",")
        (strings(0),strings(1),strings(2),strings(3),strings(4))
      })

    //获取所有学生各科信息
    val allSingleScore: List[(String, String, Int)] = allScore.map((sScore: String) => {
      val str: Array[String] = sScore.split(",")
      (str(0),str(1),str(2).toInt)
    })

    //获取各科目id对应的科目
    val cource: List[(String, String, Int)] =
      Source.fromFile("data/cource.txt").getLines().toList
      .map(s => {
        val ss = s.split(",")
        (ss(0),ss(1),ss(2).toInt)
      })

    //获取(1500100834,谷念薇,586)格式学生成绩
    val idNameScore: List[(String, String, Int)] = stu_all.map(s => {
      (s._1,s._2,cj(s._1))
    })

    //idNameScore.foreach(println)

    //按照学生成绩降序排列
    val idNameScoreSort: List[(String, String, Int)] = idNameScore.sortBy(-_._3)
    val idNameScoreSortMap = idNameScoreSort.groupBy(_._1)
    //获取前十名的学号，姓名，成绩
    val stu_10: List[(String, String, Int)] = idNameScoreSort.take(10)

    //stu_10.foreach(println)

    //获取这种格式的(1500100451,List((1000001,107), (1000002,26), (1000003,139), (1000004,2), (1000005,2), (1000006,49)))
    val stu_10_all: Map[String, List[(String, Int)]] = allSingleScore.groupBy(_._1).mapValues(s => {
      s.map(ss => {
        (ss._2,ss._3)
      })
    })


    //获取科目的Map集合
    val ncource: Map[String, List[(String, String, Int)]] = cource.groupBy(_._1)

    //获取前十名各科的成绩
    val stu_10_score: List[(String,String, List[(String, Int)])] = stu_10.map(s => {
      //学生id
      val sid: String = s._1
      //学生名字
      val sname: String = s._2
      //通过学生id获取各科的成绩
      val stuAllScore: List[(String, Int)] = stu_10_all(sid)
      //通过map获取各科成绩的课程id,与课程名进行匹配
      val cnameScore: List[(String, Int)] = stuAllScore.map(ss => {
        //学科id
        val cid: String = ss._1
        val cname: String = ncource(cid).map(_._2).head
        (cname,ss._2)
      })
      (sid,sname, cnameScore)
    })

    //stu_10_score.foreach(println)


    //求全校平均分
    val avgScore: Double = idNameScore.map(_._3).sum / idNameScore.length.toDouble
    //println(avgScore)

    //求大于平均分
    val topAvg: List[(String, String, Int)] = idNameScore.filter(s => {
      s._3 > avgScore
    })

    //topAvg.foreach(println)
    /**
      * 或者简写为:
      * val topAvg: List[(String, String, Int)] = idNameScore.filter(_._3 > avgScore)
      */


    val courceMap: Map[String, List[(String, String, Int)]] = cource.groupBy(_._1)

    //过滤掉非所有科目及格的学生
    val goodStu: List[(String, String, Int)] = idNameScore.filter(s => {
      //学生id
      val sid: String = s._1
      //学生姓名
      val sname: String = s._2
      //通过学生id获取各科的成绩
      val allCource: List[(String, Int)] = stu_10_all(sid)
      val ff: List[Int] = allCource.map(ss =>{
        //该科的总分*0.6
        val needScore: List[Int] = courceMap(ss._1).map(_._3)
        if(ss._2 >= needScore.head*0.6){
          1
        }else 0
      })
      if(ff.sum ==6 ){
        true
      } else {false}
    })

    //goodStu.foreach(println)


    val zxcv: List[(String, String, List[(io.Serializable, Int)])] = idNameScore.map(s => {
      //学生id
      val sid: String = s._1
      //学生姓名
      val sname: String = s._2
      //通过学生id获取各科的成绩
      val allCource: List[(String, Int)] = stu_10_all(sid)
      val ff: List[(io.Serializable, Int)] = allCource.map(ss =>{
        //该科的总分
        val needScore: List[Int] = courceMap(ss._1).map(_._3)
        if(ss._2 < needScore.head*0.6){
          (courceMap(ss._1).map(_._2).head+"不及格",ss._2)
        }else (courceMap(ss._1).map(_._2).head,ss._2)
      })
      (sid,sname,ff)
    })

    //zxcv.foreach(println)
    //println(zxcv.size)

    //各科得分与各科总分的比值
    val newScore: List[(String, String, Double)] = allSingleScore.map(s => {
      //学科id
      val cid: String = s._2
      val sPrecent: Double = s._3 / courceMap(cid).map(_._3).head.toDouble
      (s._1,cid,sPrecent)
    })

    //newScore.foreach(println)
    //通过学号进行分组
    val newScoreMap: Map[String, List[(String, String, Double)]] = newScore.groupBy(_._1)

    //newScoreMap.foreach(println)

    //每个学生成绩比例的平均值
    val stuAvg: Map[String, Double] = newScoreMap.map(s => {
      val sid: String = s._1
      val score: List[(String, String, Double)] = s._2
      val sum: Double = score.map(_._3).sum
      val avg = sum / s._2.size
      (sid,avg)
    })

    //id + 方差
    val id_DX: Map[String, Double] = newScoreMap.mapValues(s => {
      val a: List[Double] =s.map(ss => {
        val cz = ss._3 - stuAvg(ss._1)
        cz*cz
      })
      (a.sum / a.size)
    })

    id_DX.toList.sortBy(- _._2).take(100).map(s =>{
      idNameScoreSortMap(s._1).head
    }).foreach(println)




  }

}
