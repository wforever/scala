package com.shujia

object Demo1 {

  /**
    * def: 定义函数的关键字
    * main ： 函数名
    * args: Array[String]  :参数
    * args ： 参数名
    * Array[String]  : 参数类型  ，String ： 泛型
    * Unit : 无返回，相当于void
    *
    * 注意： main函数必须放在object中
    *
    */

  def main(args: Array[String]): Unit = {
    //println("hello")

    //var i: Int = 1

    var str = "hello,world"
    //println(str.substring(2))
    /*val strings: Array[String] = str.split(",")
    println(strings(0)+"\t"+strings(1))*/

    var str1 = "100"
    Integer.parseInt(str1)

    /*val ints: Array[Int] = Array(1,2,3,4,5,6)
    val strings = Array("hello","world","java")

    for (elem <- ints) {
      println(elem)
    }*/

    val student1 = new Student("学生",21)



    println(student1.getName)
  }
}
