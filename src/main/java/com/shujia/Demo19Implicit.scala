package com.shujia

object Demo19Implicit {

  def main(args: Array[String]): Unit = {

    def print(s: String): Unit ={
      println(s)
    }

    implicit def intToString(i: Int): String = {
      println("隐式转换方法被调用")
      i.toString
    }

    implicit def doubleToString(i: Double): String = {
      i.toString
    }

    print(100)

  }


}
