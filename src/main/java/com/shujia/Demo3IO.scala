package com.shujia

import java.io.{BufferedReader, BufferedWriter, FileReader, FileWriter}
import java.util.stream

import scala.io.{BufferedSource, Source}

object Demo3IO {

  def main(args: Array[String]): Unit = {

    /*val fileReader = new FileReader("data/students.txt")
    val reader = new BufferedReader(fileReader)
    var line = reader.readLine()

    while (line!=null){
      val strings: Array[String] = line.split(",")
      if (strings(2).toInt<22){
        println(line)
      }
      line = reader.readLine()
    }*/
    val source: BufferedSource = Source.fromFile("data/students.txt")
    val lines: Iterator[String] = source.getLines()
    for (elem <- lines) {
      println(elem)
    }

    val out: Array[String] = Source.fromFile("data/students.txt").getLines().toArray
    val writer = new BufferedWriter(new FileWriter("data/out.txt"))
    for (elem <- out) {
      println("-"+elem)
      writer.write(elem)
      writer.newLine()
      writer.flush()
    }
    writer.close()



  }

}
